class Puzzle: #using recursion methodology
    def __init__(self):
        self.simulation_num = 1 #initialising stimulation numb with 1
        self.right_order_num = 0 #right order with 0
        
    def check(self, a, i, b, j): #check function for verifying correct order
        # base cases
        if i == len(a) and j == len(b): #keeping the range
            return 0
        elif i == len(a) and j < len(b): #if a is in the range and b is out of bounds; not all elements a have been processed
            return 1
        elif i < len(a) and j == len(b): #the elements in b are out of order because not all elements in b have been processed.
            return 0
        
        if a[i] == b[j]: #if both are equal #cas1
            return self.check(a, i + 1, b, j + 1) #call the next recursion
        elif a[i] == '[': #case 2 #if a is [ and b is]
            if b[j] == ']':
                return 0 #False
            # b[j] is a number case
            b.insert(j+1, ']') #push ]
            b.insert(j, '[') #push [
            ret = self.check(a, i, b, j) #calling check function
            b.pop(j) #pop to remove the [ that has been added before
            b.pop(j + 1) #pop [ ; this is done to restore b at its original position
            return ret
        elif b[j] == '[': #checking for [
            if a[i] == ']': #check for ]
                return 1
            # b[i] is a number case
            a.insert(i+1, ']')
            a.insert(i, '[')
            ret = self.check(a, i, b, j)
            a.pop(i)
            a.pop(i + 1)
            return ret
        elif a[i] == "]" and isinstance(b[j], int): #if a is [ and b[j] is  a int
            return 1 #true
        elif isinstance(a[i], int) and b[j] == "]": #If a[i] is an integer and b[j] is '[', it returns 0.
            return 0
        else:
            if a[i] < b[j]: #if a[i] is less than bj; means the list a empties first
                return 1 #return true
            else:
                return 0 #else it is false in this case
    
    def string_to_queue(self, s): #function to convert string to integer
        s = s.strip() #remocing white spaces
        queue = [] #empty list of queue
        i = 0
        while i < len(s): #if i is less than the length of s
            if s[i] == '[' or s[i] == ']': #if there is [ or ] ; append it to queue
                queue.append(s[i])
                i = i + 1
            elif s[i] == ',': #if it is a , continue
                i = i + 1
            else:
                num = 0 #initiolizing the sum to 0
                while i < len(s) and '0' <= s[i] and s[i] <= '9':  #if i is less than the length input s #s is a digit between 0 to 9 #if either of the condition not satisfies: exit
                    num = num * 10 #if found, multiply with 10
                    num = num + int(s[i]) #and and the corresponding i and make it a int
                    i = i + 1
                queue.append(num) #appening it to queue to make alist
        return queue
            

    def simulate(self, a, b):
        left = self.string_to_queue(a) #assigning (a) to left
        right = self.string_to_queue(b) #assigning ( b) to right
        if self.check(left, 0, right, 0): #calling check function in left and right
            self.right_order_num = self.right_order_num + self.simulation_num #adding the correct order simulation numb
        
        self.simulation_num = self.simulation_num + 1 #keeping track of stimulation numb
    
    def ans(self):
        return self.right_order_num #return right order number

# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day13.txt', 'r')
Lines = file1.readlines()

# Reading the lines
i = 0
while i < len(Lines):
    a = Lines[i] #first argument
    b = Lines[i + 1] #second argument
    game.simulate(a, b) #iteration processed in stimulate function
    i = i + 3 #incrementing by 3; every 3rd line will be processed by 'a' argument and 'b' argument
    
# printing the answer
print(game.ans())

########part2########

class Puzzle:
    def __init__(self):
        self.simulation_num = 1
        self.right_order_num = 0
        self.list = [] #creating a empty list

    def check(self, a, i, b, j):
        # base cases
        if i == len(a) and j == len(b):
            return 0
        elif i == len(a) and j < len(b):
            return 1
        elif i < len(a) and j == len(b):
            return 0

        if a[i] == b[j]:
            return self.check(a, i + 1, b, j + 1)
        elif a[i] == '[':
            if b[j] == ']':
                return 0
            # b[i] is a number case
            b.insert(j + 1, ']')
            b.insert(j, '[')
            ret = self.check(a, i, b, j)
            b.pop(j)
            b.pop(j + 1)
            return ret
        elif b[j] == '[':
            if a[i] == ']':
                return 1
            # b[i] is a number case
            a.insert(i + 1, ']')
            a.insert(i, '[')
            ret = self.check(a, i, b, j)
            a.pop(i)
            a.pop(i + 1)
            return ret
        elif a[i] == "]" and isinstance(b[j], int):
            return 1
        elif isinstance(a[i], int) and b[j] == "]":
            return 0
        else:
            if a[i] < b[j]:
                return 1
            else:
                return 0

    def string_to_queue(self, s):
        s = s.strip()
        queue = []
        i = 0
        while i < len(s):
            if s[i] == '[' or s[i] == ']':
                queue.append(s[i])
                i = i + 1
            elif s[i] == ',':
                i = i + 1
            else:
                num = 0
                while i < len(s) and '0' <= s[i] and s[i] <= '9':
                    num = num * 10
                    num = num + int(s[i])
                    i = i + 1
                queue.append(num)
        return queue

    def add(self, a): #def a add function
        self.list.append(self.string_to_queue(a)) #adding the preprocessed list to the mean list

    def my_sort(self): #defining a sorting function
        n = len(self.list) #where n is a len of mean list
#bubble sort to sort the list
        for i in range(n - 1): #outer loop with range of n-1
            for j in range(0, n - i - 1): #inner loop
                if self.check(self.list[j], 0, self.list[j + 1], 0) == 0: #call check function ; if they are in wrong order
                    self.list[j], self.list[j + 1] = self.list[j + 1], self.list[j] #swap it

        first, second = 0, 0
        for i in range(len(self.list)):#looping in the range of len of mean list
            if (''.join(map(str, self.list[i])) == "[[2]]"): #The join function is used to convert each inner list to a string, which is then compared to the target string using the equality operator ==.
                first = i + 1#store that iteration/ pos
            elif (''.join(map(str, self.list[i])) == "[[6]]"):
                second = i + 1 #get the pos

        return first * second #product of pos


# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day13.txt', 'r')
Lines = file1.readlines() #reading the lines

game.add("[[2]]") #adding
game.add("[[6]]") #adding

# Reading movement of rope line by line
i = 0
while i < len(Lines):
    game.add(Lines[i])
    game.add(Lines[i + 1])
    i = i + 3

print(game.my_sort())