class Puzzle:
    def __init__(self): #defining function
        self.HeadX = 1 #initialising coordinate x of head
        self.HeadY = 1 #initialising  coordinate y of  head
        self.TailX = 1 #initialising coordinate X of tail
        self.TailY = 1 #initialising coordinate y of tail
        self.visited = set() #visited set for unique positions
        # as Tail is already started at (1, 1)
        self.visited.add((1, 1)) #adding it to visited set

    def simulate(self, direction, moves): #defining a function stimulate with our direction and moves
        for _ in range(moves):
            # Creating copy of current position of Head(X, Y) to update for new position of it
            Head_newX, Head_newY = self.HeadX, self.HeadY
            
            # adding step to current positon of Head(X, Y) to find new position
            if (direction == "L"): Head_newX = Head_newX - 1 #for LEFT SIDE
            elif (direction == "R"): Head_newX = Head_newX + 1 #FOR RIGHT
            elif (direction == "U"): Head_newY = Head_newY + 1 #FOR UP
            elif (direction == "D"): Head_newY = Head_newY - 1 #FOR DOWN

            # Moving Tail if new position of Head and Tail are not adjacent
            if abs(Head_newX - self.TailX) >= 2 or abs(Head_newY - self.TailY) >= 2: #DISTANCE BETWEEN THE  NEW POS OF HEAD AND CURRENT POS OF TAIL BECOMES GREATER THAN 2 IN ANY ONE OF THE CONDITION; ALSO DIRECTION CAN BE NEGITIVE
                self.TailX = self.HeadX #UPDATING POSITION OF TAIL
                self.TailY = self.HeadY
                
            self.HeadX = Head_newX #UPDATING NEW COORDINATE OF HEAD
            self.HeadY = Head_newY
            self.visited.add((self.TailX, self.TailY)) #IN EACH ITERATION ADDING EACH UNIQUE COORDINATE OF TAIL

    def ans(self):
        return len(self.visited) #LENGTH OF THE VISITED SET =OUTPUT


# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day09.txt', 'r')
Lines = file1.readlines()

# Reading movement of rope line by line
for line in Lines: #reading the lines
    direction = line.split()[0] #assigning variable for directions which is the first one
    moves = int(line.split()[1]) #and assignning the numb of moves variable to numb and converting to int
    game.simulate(direction, moves)  #assigning variable to function

print(game.ans())

####### PART2#####
class Puzzle:
    def __init__(self):  # CREATING COORDINATES OF 10 NODES
        self.X = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # COORDINATES OF EACH NUMB AS A list (h<-1<-2<-3 ....9)
        self.Y = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        self.visited = set()  # visited set for unique points
        # as Tail is already started at (1, 1)
        self.visited.add((1, 1))

    def simulate(self, direction, moves):
        for _ in range(moves):
            newX = self.X.copy()  # creating final coordinates of each points
            newY = self.Y.copy()

            # adding step to current positon of Head(X, Y) to find new position
            if (direction == "L"):
                newX[0] = newX[0] - 1  # at index 0 moving head left
            elif (direction == "R"):
                newX[0] = newX[0] + 1  # at index 0 moving head right
            elif (direction == "U"):
                newY[0] = newY[0] + 1  # at index 0 moving head up
            elif (direction == "D"):
                newY[0] = newY[0] - 1  # at index 0 moving head down

            for i in range(9):  # loopinh through i =0 to 8
                # adding step to current positon of Head(X, Y) to find new position
                if abs(newX[i] - self.X[i + 1]) >= 2 and abs(newY[i] - self.Y[
                    i + 1]) >= 2:  # coordinate of new parent node - coordinate of cuurent child node
                    newX[i + 1] = newX[i + 1] + (1 if (newX[i] - self.X[
                        i + 1]) >= 2 else -1)  # if yes, updating pos of child  node #newcordinate of parent - old value of child
                    newY[i + 1] = newY[i + 1] + (1 if (newY[i] - self.Y[i + 1]) >= 2 else -1)
                elif abs(newX[i] - self.X[
                    i + 1]) >= 2:  # for the condition when x coordinate gets updates/changes(child); 1:for right direction; -1 for left direction
                    newX[i + 1] = newX[i + 1] + (1 if (newX[i] - self.X[i + 1]) >= 2 else -1)
                    newY[i + 1] = newY[i]
                elif abs(newY[i] - self.Y[i + 1]) >= 2:  # for the condition when y coordinate gets updated (child)
                    newX[i + 1] = newX[i]
                    newY[i + 1] = newY[i + 1] + (1 if (newY[i] - self.Y[i + 1]) >= 2 else -1)

                    # updating new coordiantes of nodes
            self.X = newX
            self.Y = newY
            self.visited.add((self.X[9], self.Y[9]))  # adding the 9th curent coordinate of 9 to visited

    def ans(self):
        return len(self.visited)  # length of set visited


# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day09.txt', 'r')
Lines = file1.readlines()

# Reading movement of rope line by line
for line in Lines:
    direction = line.split()[0]
    moves = int(line.split()[1])
    game.simulate(direction, moves)  # function to call

print(game.ans())

