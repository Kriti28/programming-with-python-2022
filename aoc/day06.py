with open("day06.txt", "rt") as myfile:
    lines = myfile.readlines()

line = lines[0]


for i in range(3, len(line)): # we are running a loop from 4th to the length of line as in end of line
    if len(set(line[i-3: i+1])) == 4: #iterating over start and end position of first 4 leters len({'a', 'b', 'c', 'd'}) = 4;  line[0:4] = 'aaab', line[1:5] = 'aabc',and so on #set will give unique characters in the mentioned range
                                      # and len will give the respective length len({'a', 'b'}) = 2
        print(i+1) #if found the position is printed
        break

####part2######
with open("day06.txt", "rt") as myfile:
    lines = myfile.readlines()

line = lines[0]


for i in range(13, len(line)): # we are running a loop from 14 to the length of line as in end of line

    if len(set(line[i-13: i+1])) == 14: #considering same for 14 letters in length
        print(i+1)
        break