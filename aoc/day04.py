with open("day04.txt", "rt") as myfile:
    lines = myfile.readlines()

    sum = 0
    for line in lines:
        assignments = line.strip('\n').split(',') #stri[[png at /n and splitting at , ; making it a assignment like     so assignments = ["14-65", "23-45"]

        assignment1 = assignments[0].split('-') #splitting at - in first element
        assignment2 = assignments[1].split('-') #splitting at - in second element

        assignment1 = [i for i in range(int(assignment1[0]), int(assignment1[1]) + 1)] #for every number in range ;adding it to list and assignning it to assignment 1 and 2 respectively  and making it a integer int[];
                                                                                    # int(assignment1[0]) = int("14") and int(assignment1[1]) = int("65") and making a list o f 14 to 65 adding 1 to take last numb in account

        assignment2 = [i for i in range(int(assignment2[0]), int(assignment2[1]) + 1)]
#checking if the assignment1 is a subset of assignment2 or vice versa
        if set(assignment1).issubset(set(assignment2)) or set(assignment2).issubset(set(assignment1)): #if any one of the condition is true the sum will count
            sum += 1 #adding the sum
print(sum)


######Part2#######
with open("day04.txt", "rt") as myfile:
    lines = myfile.readlines()

    sum = 0
    for line in lines:
        assignments = line.strip('\n').split(',')

        assignment1 = assignments[0].split('-')
        assignment2 = assignments[1].split('-')

        assignment1 = [i for i in range(int(assignment1[0]), int(assignment1[1]) + 1)]
        assignment2 = [i for i in range(int(assignment2[0]), int(assignment2[1]) + 1)]

        if set(assignment1) & set(assignment2): #and operator will find out any common integer between the 2 lists
            sum += 1

print(sum)