class Puzzle:
    def __init__(self):
        self.row = 2000000 #check for the mentioned row
        self.not_possible_space = 0 # to find
        self.ranges = [] #
        self.bacons = set() #storing the coordinates of beacon in set so that it does not repeat

    def simulate(self, Sx, Sy, Bx, By): #function for stimulate
        if By == self.row: #condition in which the coordinate of Beacon is = self row
            self.bacons.add(Bx) #add the x coordinate of it
        
        manhattan_dist = abs(Sx - Bx) + abs(Sy - By) #cal manhattan dist
        if abs(Sy-self.row) <= manhattan_dist:  #if the distance is in the proximity
            delta = manhattan_dist - abs(Sy-self.row) #finding the diff between manhattan and (sy and self.row)
            self.ranges.append((Sx - delta, Sx + delta)) #finding  the coordinates of possible visiting coordinates and appending in the range from left to right

    def ans(self): #def a ans function for merging the ranges
        self.ranges.sort() #sorting the ranges
        n = len(self.ranges)
        i = 0
        while i < n:
            low, high = self.ranges[i]
            i = i + 1
            while i < n and self.ranges[i][0] <= high:
                high = max(high, self.ranges[i][1])
                i = i + 1
            self.not_possible_space = self.not_possible_space + (high - low + 1) #updating the count with the len of a range at each iteration
        return self.not_possible_space - len(self.bacons) #subtracting the beacons already there from the result


# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day15.txt', 'r')
Lines = file1.readlines()

for line in Lines: #reading data input fo rcoordinates of s and b
    l = line.split()
    Sx = int(l[2].split("=")[1][:-1])
    Sy = int(l[3].split("=")[1][:-1])
    Bx = int(l[8].split("=")[1][:-1])
    By = int(l[9].split("=")[1])
    game.simulate(Sx, Sy, Bx, By)

print(game.ans())

####part2#####

class Puzzle:
    def __init__(self):
        self.row = -1
        self.ranges = []
        self.bacons = set()

    def simulate(self, Sx, Sy, Bx, By):
        if By == self.row:
            self.bacons.add(Bx)

        manhattan_dist = abs(Sx - Bx) + abs(Sy - By)
        if abs(Sy-self.row) <= manhattan_dist:
            delta = manhattan_dist - abs(Sy-self.row)
            self.ranges.append((Sx - delta, Sx + delta))

    def ans(self):
        self.ranges.sort()
        n = len(self.ranges)
        last = -1
        i = 0
        while i < n: #merging the ranges  here
            low, high = self.ranges[i]
            i = i + 1
            while i < n and self.ranges[i][0] <= high:
                high = max(high, self.ranges[i][1])
                i = i + 1
            if low > 4000000: #if the low is out of our constraint
                return -1  #return false if out of bounds
            elif last + 1 < low: #check whether the last value of prev range is less than the current low
                return last + 1 #if found give the point
            last = high #which is a variable high
        if last + 1 <= 4000000: #setting the constraint for high
            return last + 1 #if found
        return -1 #if not return zero

# Creating instance of Puzzle


# Read input files
file1 = open('day15.txt', 'r')
Lines = file1.readlines()

for x in range(0, 4000001):
    game = Puzzle()
    game.row = x
    for line in Lines:
        l = line.split()
        Sx = int(l[2].split("=")[1][:-1])
        Sy = int(l[3].split("=")[1][:-1])
        Bx = int(l[8].split("=")[1][:-1])
        By = int(l[9].split("=")[1])
        game.simulate(Sx, Sy, Bx, By)

    if x % 100000 == 0:
        print("Running: ", x)

    res = game.ans()
    if (res != -1):
        print(4000000 * res + x)
        break
