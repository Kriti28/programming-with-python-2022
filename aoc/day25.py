import math

data = '''1=-0-2
12111
2=0=
21
2=01
111
20012
112
1=-1=
1-12
12
1=
122'''

with open("day25.txt",'rt') as file:
    data = file.read().strip().split('\n')

# create dictionary for lookup
dict_enc = {
	'2': 2, 
	'1': 1, 
	'0': 0, 
	'-': -1,
	'=': -2
}

# create a reverse dictionary for reverse lookup to convert to SNAFU
dict_enc_rev = {value:key for key, value in dict_enc.items()}

tots = []
#iterate over all SNAFU
for item in data:
	len_item = len(item)  #length to use for power of 5 eg : 1=-0-2
	enc_ = [5**i for i in range(len_item-1, -1, -1)]	# create power of 5 in a list
	items = list(item)   # ["1","=","-","0","-","2"]
	tot = 0
	for i, j in list(zip(items, enc_)):  # zip both lists to multiply power of 5 with encoding character
		tot += dict_enc[i]*j  #multiply encoding and power of 5 and add them all
	tots.append(tot) # append to the list
	
print(tots)
print(sum(tots))  #take the sum of the list
	
tot = sum(tots)

pow5 = 0
snafu = ""

while tot > 0:
	c = 5**pow5 #current power of 5
	n = 5**(pow5+1) #next higher power of 5

	# get remainder from both n and -n to get the smallest remainder with c
	r1 = (tot%n)//c
	r2 = (tot%-n)//c

	#based on the smaller remainder , we create SNAFU
	if math.fabs(r1) > math.fabs(r2): # if r1 is larger, use r2
		r2 = -2 if r2 < -2 else r2
		snafu +=  dict_enc_rev[r2]
		tot -= tot%-n
	else:  	# else use r1
		r1 = 2 if r1 > 2 else r1
		snafu += dict_enc_rev[r1]
		tot -= tot%n

	pow5 += 1

print(snafu[::-1])