with open("day1 input.txt", "rt") as myfile:
    nospace=[]
    lines = myfile.readlines()

for i in lines: #reading the lines
   j = i.replace('\n','') #replacing \n to space
   nospace.append(j) #appending the data in empty list

totalall=[]
total = 0

for item in nospace: #looping in the data
    if item != '': #checking if item is not equal space
        item = int(item) #taking it as int
        total = total+item #incrementing the total
    else:
        totalall.append(total) #else appending into totalall empty list
        total = 0

totalall.append(total) #the total is now appending in the overall total
totalall.sort(reverse=True) #sorting the list in descending order
highest = totalall[0]  #now at 0 pos the highest numb
second_highest = totalall[1] #second highest
third_highest = totalall[2] #third highest
print(highest) #highest numb
print(highest, second_highest,third_highest)

top_three_sum = highest + second_highest+third_highest #calculating the sum

print(top_three_sum) #prinitng the sum of 3 highest