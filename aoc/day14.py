class Puzzle:
    def __init__(self):
        self.space = [['.' for _ in range(1000)] for _ in range(1000)] #creating a matrix of " .",
        self.space[0][500] = '+' #creating a source of sand
        self.lowest_block = 0
        
    def add_rock(self, paths): #def a func add rock
        x0, y0 = -1, -1 #initializing with -1
        for path in paths: #in for loop if it is a ->
            if path == "->":
                continue
            #if they are not the If the current path is not "->", the x and y coordinates of the rock are extracted by splitting the path by "," and converting the two resulting strings to integers.
            x, y = int(path.split(",")[1]),int(path.split(",")[0])
            
            self.lowest_block = max(self.lowest_block, x)#taking the list of path as input and updates the valeu of lowest_block
            if x0 == -1 and y0 == -1: #If both x0 and y0 are -1, it means this is the first rock being added, so the x0 and y0
                                     # are updated to the x and y coordinates of the rock.
                x0, y0 = x, y
            else: #if not equal to -1
                if x0 == x: #the code checks if x0 is equal to x. If x0 is equal to x, then it's assumed that the rock is being added vertically,
                            # and a loop is used to fill the space between the previous rock and the current rock with "#" symbols
                    for j in range(min(y0, y), max(y0, y) + 1):
                        self.space[x0][j] = "#"
                else:
                    # Case where y0 = y the rock is being added horizontally and
                    # a similar loop is used to fill the space between the previous rock and the current rock with "#" symbols.
                    for i in range(min(x0, x), max(x0, x) + 1):
                        self.space[i][y0] = "#"
            x0, y0 = x, y #x0 and y0 values are updated to the x and y coordinates of the current rock.
            
    # return true if sand drops belowe lowest point
    # return false if sand drops stop at some point
    def drop(self, x, y): #def a drop function; x and y, which represent the current location of the sand drop.
        # base case
        if x > self.lowest_block: # if x is greater than the lowest block
            return True #indicating that the sand has fallen off the bottom of the simulation space.
        
        # case where bottom is not blocked
        if self.space[x+1][y] == ".": #If the space at the location [x+1][y] is not a "."
            return self.drop(x+1, y)
        # case where it will check for bottom is block by "#" - stone or "O" - another sand drop
        elif self.space[x+1][y] == "#" or self.space[x+1][y] == "O": #If either of these spaces is open, the method calls itself recursively with the updated x and y values.
            if self.space[x+1][y-1] == ".": #f both [x+1][y-1] and [x+1][y+1] are blocked, the method marks
                                               # the current space with an "O" symbol and returns False.#checking diagonally left
                return self.drop(x+1, y-1)
            elif self.space[x+1][y+1] == ".": #if empty diagonally right #if empty
                return self.drop(x+1, y+1)   #put sand over there
            else:
                self.space[x][y] = "O" #the pos found will be fixed
                return False #and return false so that it does not move further
    
    def simulate(self):
        # base case  #If there is sand just below the starting point , the function returns 0, as the sand would not flow in this case.
        if self.space[1][500] == "#":
            return 0
        
        sand_drop = 0 #count for sand drop
        while True: #If there is no rock at the initial location, the code enters a while loop that continues until the drop() method returns True.
            if self.drop(1, 500):
                break
            sand_drop = sand_drop + 1 #incrementing at each iteration
        return sand_drop #retuen total numb


# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day14.txt', 'r')
Lines = file1.readlines()

# Reading movement of rope line by line
for line in Lines:
    game.add_rock(line.split()) #adding rock

print(game.simulate())

######part2#####
class Puzzle:
    def __init__(self):
        self.space = [['.' for _ in range(1000)] for _ in range(1000)]
        self.space[0][500] = '+'
        self.lowest_block = 0

    def add_rock(self, paths):
        x0, y0 = -1, -1
        for path in paths:
            if path == "->":
                continue

            x, y = int(path.split(",")[1]), int(path.split(",")[0])

            self.lowest_block = max(self.lowest_block, x)
            if x0 == -1 and y0 == -1:
                x0, y0 = x, y
            else:
                if x0 == x:
                    for j in range(min(y0, y), max(y0, y) + 1):
                        self.space[x0][j] = "#"
                else:
                    # Case where y0 = y
                    for i in range(min(x0, x), max(x0, x) + 1):
                        self.space[i][y0] = "#"
            x0, y0 = x, y

    # return true if sand drops below lowest point
    # return false if sand drops stop at some point
    def drop(self, x, y):  # def func for coordinates of sand drop
        # base case #look into this
        if self.space[x][y] == "O":  # if current coordinates occupied by sand drop; true
            return True

        if x == self.lowest_block + 1:  # if the current height of the sand drop is equal to the "lowest_block" plus one. If it is, the sand drop has reached the bottom of the simulation space, and
            # the current coordinate is marked as "O" to indicate it is occupied by sand
            self.space[x][y] = "O"
            return False

        # case where bottom is not blocked
        if self.space[x + 1][y] == ".":
            return self.drop(x + 1, y)
        # case where bottom is block by "#" - stone or "O" - another sand drop
        elif self.space[x + 1][y] == "#" or self.space[x + 1][y] == "O":
            if self.space[x + 1][y - 1] == ".":
                return self.drop(x + 1, y - 1)
            elif self.space[x + 1][y + 1] == ".":
                return self.drop(x + 1, y + 1)
            else:
                self.space[x][y] = "O"
                return False

    def simulate(self):  # This is a simulation of sand dropping from a certain height onto a flat surface.
        # base case
        sand_drop = 0
        while True:
            if self.drop(0, 500):  # if the drop in the arguments
                break  # returns true breaks #false loop continue sand dropping
            sand_drop = sand_drop + 1
        return sand_drop


# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day14.txt', 'r')
Lines = file1.readlines()

# Reading movement of rope line by line
for line in Lines:
    game.add_rock(line.split())

print(game.simulate()) 