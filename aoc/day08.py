with open("day08.txt",'rt') as file:
    data = file.read().strip().split('\n') #reading the file
l=[] #making an empty list
for i in data:
    l.append([int(j) for j in list(i)]) #making the elements from data and converting it to list ; appendind it to l

#l=[[3,0,3,7,3],[2,5,5,1,2],[6,5,3,3,2],[3,3,5,4,9],[3,5,3,9,0]]
s=0 #initialising s with zero
for i in range(1,len(l)-1):                #taking the sec row(As the border ones are visible from outside)till the length and subtracting one to take second last element into account
    for j in range(1,len(l[i])-1):         # FOR loop for COLUMN; iterating over all the columns within i; taking range wrt to the border rules
        c=0
        a=0
        b=0
        d=0
        for k in range(0,j):             #iterarting over column and taking range from first to all the elements
            if l[i][k]>=l[i][j]:           #left hand side of column, zero is left hand
                a=1
                break
        for k in range(j+1,len(l[i])):
            if l[i][k]>=l[i][j]:           # right hand side of column
                b=1
                break
        for k in range(i+1,len(l)):
            if l[k][j]>=l[i][j]:           # below of column
                c=1
                break
        for k in range(0,i):
            if l[k][j]>=l[i][j]:           # top rows above the column
                d=1
                break
        if a == 0 or b == 0 or c == 0 or d == 0: #if for all the above conditions if a,b,c,d is equals to zero
            # print(l[i][j],i,j) #or if one is satisfied it should inc
            s = s + 1 #incrementing s
s = s + 99 * 4 - 4 # adding the number of columns and subtracting the common 4 corners
print(s)


#part2
#l=[[3,0,3,7,3],[2,5,5,1,2],[6,5,3,3,2],[3,3,5,4,9],[3,5,3,9,0]]
s=1 #counting the score
l1=[]
for i in range(1,len(l)-1): #loop within the internal numb of rows
    for j in range(1,len(l[i])-1):  #considering the internal columns
        a=0
        b=0
        c=0
        d=0
        for k in range(j-1,-1,-1):# loop starts from the left side of element until one less than the l and it starts from backwards
            a=a+1
            if l[i][k]>=l[i][j]: #if found than we will add it to a
                break
        for k in range(j+1,len(l[i])): #checking for right side
            b=b+1
            if l[i][k]>=l[i][j]:
                break
        for k in range(i+1,len(l)):#checking for bottom side
            c=c+1
            if l[k][j]>=l[i][j]:
                break
        for k in range(i-1,-1,-1): #checking for top and sorting in backwards
            d=d+1
            if l[k][j]>=l[i][j]:
                break
        #print(a,b,c,d)
        l1.append(a*b*c*d)#appending it to the list and multiplying the scores
max(l1)
print(max(l1)) #finding the max value



