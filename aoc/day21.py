from sympy import symbols
from sympy.solvers.solveset import linsolve

s = '''root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32'''


# function to perform the operation based on the "operation string" written in the data
def return_op(m1, op, m2, useful_monkeys):
    if op ==  '/': 
        return useful_monkeys[m1] / useful_monkeys[m2]
    elif op == '+': 
        return useful_monkeys[m1] + useful_monkeys[m2]
    elif op == '-': 
        return useful_monkeys[m1] - useful_monkeys[m2]
    elif op == '*': 
        return useful_monkeys[m1] * useful_monkeys[m2]



# cmd = s.split('\n')

with open("day21.txt",'rt') as file:
    cmd = file.read().strip().split('\n')

not_useful_monkeys = {}
useful_monkeys = {}

# this loop fills useful and not useful monkey dictionaries 
# with data.
for i in cmd:
    monkey = i[:4]  # monkey name
    trail  = i[5:].strip() # after colon rest is stored in trail
    
    if trail.isdigit():
        useful_monkeys[monkey] = int(trail)
    else:
        not_useful_monkeys[monkey] = [trail[:4], trail[5], trail[7:]] # make a list with 3 items : monkey1, operation , moneky2

useful_monkeys_keys = list(useful_monkeys.keys())

# print(not_useful_monkeys)
# print()
# print(useful_monkeys)

# we run a loop until root monkey is added to the useful monkey dictionary
# adding to the useful monkey dict. means that root has now been calculated
while 'root' not in useful_monkeys_keys:
	# iterate over the dict. key, value
    for nu_monkey, item in not_useful_monkeys.items():
            m1, op, m2 = item[0], item[1], item[2]
            # check if monkey1 and monkey2 belong to the useful monkeys
            if m1 in useful_monkeys_keys and m2 in useful_monkeys_keys:
                result = return_op(m1, op, m2, useful_monkeys)
                # print(m1, op, m2, result)
                useful_monkeys[nu_monkey] = result
                useful_monkeys_keys.append(nu_monkey)

print(useful_monkeys['root'])            



#####part 2#####
m1, m2 = not_useful_monkeys['root'][0], not_useful_monkeys['root'][2],   #monkeys of root
m1_v, m2_v = useful_monkeys[m1], useful_monkeys[m2]

#check between the above monkeys, which one gets its number from 'humn'
def check_humn(m):
    if m == 'humn':
        return True
    else:
        if m in not_useful_monkeys.keys():
            x, _ , y = not_useful_monkeys[m]
            if check_humn(x) == True or check_humn(y) == True:
                return True

# recursively go from monkey m to form the symbolic expression for "humn"
def return_exp(m):
    if m in list(useful_monkeys.keys()): # if the monkey is in useful_monkeys, retunr the number
        return useful_monkeys[m]
    elif m in not_useful_monkeys.keys(): # get both monkeys from not_useful _monkeys
        x, op , y = not_useful_monkeys[m]
        xv = return_exp(x)              # recursively go to both monkey
        yv = return_exp(y)
        # once we get the values from both monkeys , then create symbolic expression
        res = return_op(x, op, y, useful_monkeys)
        #update the expression in useful monkey
        useful_monkeys[m] = res
        return res
        
#reset monkey dict. to create symbolic expression for humn
not_useful_monkeys = {}
useful_monkeys = {}
for i in cmd:
    monkey = i[:4]  # monkey name
    trail  = i[5:].strip() # after colon rest is stored in trail
    
    if trail.isdigit():
        useful_monkeys[monkey] = int(trail)
    else:
        not_useful_monkeys[monkey] = [trail[:4], trail[5], trail[7:]] # make a list with 3 items : monkey1, operation , moneky2
##

useful_monkeys['humn'] = symbols('humn')

if check_humn(m1):

    exp = return_exp(m1)
    exp = m2_v - exp  # create final symbolic expression to solve
    print(linsolve([exp], (useful_monkeys['humn']))) # use a linear solver to solve the equation for humn


if check_humn(m2):

    exp = return_exp(m2)
    exp = m1_v - exp
    print(linsolve([exp], (useful_monkeys['humn'])))


