with open("day03.txt", "rt") as myfile:
    lines = myfile.readlines()
print(lines) #reading the lines

alpha_dict = {} #creating  a alpha dict {'a': 1, 'b': 2, 'c': 3, ..... 'y': 25, 'z': 26}
ch = 'a'

for i in range(1,27):
    alpha_dict[ch] = i
    ch = chr(ord(ch) + 1) # increasing char, 'a' => 'b'; numerical value of ch

sum = 0
for line in lines:
    line = line.strip('\n') #Removing newline garbage char
    half_length = int(len(line)/2) #Getting half length of the line
    first_half = line[:half_length] #First half of the line
    second_half = line[half_length:] #Second half of the line

    x = set(first_half)&set(second_half) #putting the first and sec half into the set 
    x = ''.join(x) #Converting common char from set to string

    if x.islower():  #if X is a lower case 
        sum = sum + alpha_dict[x] #adding the correspnding score from the dictionary
    else: #else it is a upper case
        sum = sum + alpha_dict[x.lower()] +26 #adding 26 to the lowercase to find upper case score

print(sum)

######part2#######

with open("day03.txt", "rt") as myfile:
    lines = myfile.readlines()

alpha_dict = {} #making a dict
ch = 'a'

for i in range(1, 27):
    alpha_dict[ch] = i
    ch = chr(ord(ch) + 1)

sum = 0
i = 1 #first line of the of data
groupOfThree = [] #empty list to  make group of three

for line in lines:
    groupOfThree.append(set(line.strip('\n'))) #strip at nect line and appending it to group of three

    if i % 3 == 0: #if reminder is 0; making a throup of 3 at every 3 lines
        set_intersection = groupOfThree[0] & groupOfThree[1] & groupOfThree[2] #assigning them as group of three in the variable
        set_intersection = ''.join(set_intersection) #making it a string for key in dictionary

        if set_intersection.islower(): #if it is in lower case
            sum += alpha_dict[set_intersection] #add the priority numb to sum
        else:
            sum += alpha_dict[set_intersection.lower()] + 26 #else in upper case add 26 to get the priority numb of the alphabat

        groupOfThree = [] #empties the list

    i +=1 #incrementing at the end of iteration

print(sum)


