class Puzzle:
    def __init__(self): #defining a function
        self.X = 1 #initialising with 1
        self.queue = [] #creating a queue list

    def simulate(self, cycles, V): #cycles are number of cycles and V is the value for operation
        self.queue.append((cycles, V)) #appending it to queue
    
    def ans(self):
        signal_strength = 0 #overall strength initialising
        breaks = [20, 60, 100, 140, 180, 220] #mentioned signals
        
        i = 0 #current sec
        while i<=220 and len(self.queue)!=0: #loop until 220 and the queue is empty
            front = self.queue.pop(0) #at front we have the first queue element
            cycles, V = front[0], front[1]
            
            for _ in range(cycles):
                i = i + 1 #incrementing the cycle
                if i in breaks: #if that cycle is in breaks
                    signal_strength = signal_strength + i * self.X #ignal strength + cycle * X
                    
            self.X = self.X + V  #adding the int to initial x
        return signal_strength


# Creating instance of Puzzle
game = Puzzle()

# Read input files
file1 = open('day10.txt', 'r')
Lines = file1.readlines()

# Reading movement of rope line by line
for line in Lines:
    L = line.split()
    if (L[0] == "noop"): #if noop and cycle 1 and do noting 0
        game.simulate(1, 0)
    else:
        game.simulate(2, int(L[1])) #else do 2 cycles and make it int

# printing the answer
print(game.ans())