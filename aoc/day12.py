import math #import

with open("day12.txt",'rt') as file:
    data = file.read().strip().split('\n') #reading the file

# make a mxn grid  ; begin by initialising a list
grid = []

# we make the grid of shape nx * ny
for item in data:
    grid.append(list(item)) #appending the item into data

# store the dimensions of the grid to control boundary conditions
nx , ny = len(grid), len(grid[0])
# initialize the corrdinates for position S and position E ;
si, sj, ei, ej = 0,0,0,0
# set a variable to find the total number of steps taken to reach E
t = 0
# ff is a flag to get out of the loop when E is reached
ff = False

# find the corrdinates of S and E
# replace S and E with their corresponding elevation levels
for i in range(nx):
    for j in  range(ny):
        if grid[i][j] == 'S':
            si, sj = i, j
            grid[i][j] = 'a' # set elevation level of S by replacing it with 'a'
        if grid[i][j] == 'E':
            ei, ej = i, j
            grid[i][j] = 'z' # set elevation level of E by replacing it with 'z'

print(si, sj)
print(ei, ej)

# create elevation directory using a directory
# this is used to store elevation levels
# these are later used to condition how to travel in the grid
elevation_lvl = "abcdefghijklmnopqrstuvwxyz" # elevation tags
elevation_dict = {}  #store elevation lvl for each elevation tag starting with 1 for a , 2 for b , 3 for c and so on...

for i, lvl in enumerate(elevation_lvl, 1):
    elevation_dict[lvl] = i   # way to initialize key-value pairs in a dictionary

print(elevation_dict)
print()

# this function returns a list of the 4 coordinates on which can make a move to
def get_moves(posi, posj):
    l =  [(posi+1, posj), (posi-1, posj), (posi, posj+1), (posi, posj-1)]
    return l

# this function takes two coordinates as input
# then checks the elevation tag in the grid using those coordinates
# then returns boolean based on whether the elevation lvl is at most one higher or not
def check_elevation(current_c, new_c):
    a, b = current_c[0], current_c[1]
    c, d = new_c[0], new_c[1]  # coordinates of new move

    if elevation_dict[grid[c][d]] - elevation_dict[grid[a][b]] <= 1:
        return True
    return False


# create 2 lists
# moves list contain a tuple(a, b, c) ; a stores the total number of moves made so far to the coordinates (b, c)
moves = [(0, si, sj)]
# contains coordinates that have already been visited
visited = [(si, sj)]


# keeping moving till there are moves in the list
while moves:
    t, i, j = moves.pop(0)   # get the move and then remove it from the list
    for new_move in get_moves(i, j)	:
        if new_move[0] < 0 or \
           new_move[0] >= nx or \
           new_move[1] < 0 or \
           new_move[1] >= ny or \
           new_move in visited or \
           not check_elevation((i, j), new_move): #if not in the elevation it will continue=False
            continue
        if new_move[0] == ei and new_move[1] == ej:
            print(t + 1)
            ff = True #flagging it once we get our coordinates for end position
        visited.append(new_move) #appending into list
        moves.append((t + 1, new_move[0], new_move[1])) #appending it to moves to fetch it later
	
    if ff:
        break
        
        
#part 2

# create starting position as E now
moves = [(0, ei, ej)] # set it as first move
visited = [(ei, ej)]  # set it as first visited


while moves:
    t, i, j = moves.pop(0)
    for new_move in get_moves(i, j)	:
        if new_move[0] < 0 or\
           new_move[1] < 0 or \
           new_move[0] >= nx or \
           new_move[1] >= ny or \
           new_move in visited or \
           not check_elevation(new_move, (i, j)):
            continue
        if grid[new_move[0]][new_move[1]] == "a":  # iterate until we find the first "a" and this will be the closest
            print(t + 1)  # print the moves it took to reach the first "a" and print
            exit(0)
        visited.append(new_move)
        moves.append((t + 1, new_move[0], new_move[1]))
        
        

