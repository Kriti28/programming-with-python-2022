import math

class LinkListNode:
    def __init__(self, x, index = 0):
        self.x = x
        self.index = index   # MY IDEA
        self.next = None
        self.back = None

# this function creates a circular link list
def create_ll(data): #making a circle list
    node = LinkListNode(data[0], 0)
    head = node

    for i in range(1, len(data)):
        new_node = LinkListNode(data[i], i)
        node.next = new_node
        new_node.back = node
        new_node.next = head
        node = node.next

    head.back = node
    return head
# print the circular link list
# stopping criteria for printing is when the node's next node is same as head
# head -> node1 -> node2 -> node3 -> head
def print_ll(node):
    head = node
    while node.next != head:
        print((node.x, node.index), end = " ")
        node = node.next
    print((node.x, node.index))

# from a reference node start searching for a node with a particular index
def get_node_at_index(head, index):
    node = head
    while node.index != index :
        node = node.next

    return node

# this function will remove node at a particular index from reference node (head)
def remove_node(head, index):
    node = head
    data = 0

    # if the node to remove is the HEAD node itself
    # then while removing it is important to assign a HEAD node again
    if index == 0:
        data = head.x
        head = head.next

    while index != 0:
        node = node.next
        data = node.x
        index -= 1

    node.back.next = node.next
    node.next.back = node.back

    return head, data

# this function adds a node in reference to the HEAD node
def add_node(head, indx_2_move, data, node_indx) :
    new_node = LinkListNode(data, node_indx)
    node = head

    # indx2move stores how many indexes to move with reference to head
    # if indx2move is +ive then move right i.e node-> next
    # else move left i.e node->back
    if indx_2_move > 0 :
        while indx_2_move > 1:
            node = node.next
            indx_2_move -= 1
    else:
        while indx_2_move <= 0:
            node = node.back
            indx_2_move += 1

    # once the node is found
    # add the new_node after it
    new_node.next = node.next
    node.next = new_node
    new_node.back = node
    new_node.next.back = new_node

with open("day20.txt",'rt') as file:
    d = file.read().strip().split('\n')

d = list(map(int , d))

len_l = len(d)
print(len_l)

head = create_ll(d)

for _ in range(1): # this is done for mixing 10 times
    # for each node.index we have to now move the node
    # node.index stores the order in which the node are supposed to move
    # if the order changes the result will change
    for index in range(len(d)):
        # first get the node which we are supposed to move
        # assign the node as head node for ease
        head = get_node_at_index(head, index)
        # if data in the node is not equal to zero only then move the node
        if head.x != 0 :
            # remove the node at the index
            head, data = remove_node(head, 0)
            # we use modulus operation to find the number of moves for the node
            # indx2move stores how many steps the node will move
            if math.fabs(data) > len_l: #check if the data is greater than the list size
                indx_2_move = math.fabs(data) % (len_l-1)
                indx_2_move = indx_2_move * -1 if data < 0 else indx_2_move
            else:
                indx_2_move = data  # if data is not greater than list size then ind2move is the data itself

            # print(f"removed {data}, index2move {indx_2_move}")
            add_node(head, indx_2_move , data, index) # now we add the node with reference to head . index is passed to the function to preserve the original order in which nodes are required to mix
            # print_ll(head)
    print("round of mixing done")
    

# find the node with data  = 0
node0 = head
while node0.x != 0:
    node0 = node0.next

a, b, c  = 0, 0, 0
node = node0
# this loop finds the node data with reference to node(zero) AT 1000TH, 2000TH, 3000TH place
for i in range(1, 3001):
    if i == 1001:
        a = node.x
    if i == 2001:
        b = node.x
    if i == 3000:
        c = node.next.x

    node = node.next

print("sum part 1")
print(a+b+c)

# part 2

d = [i * 811589153 for i in d]  # 811589153 belongs to part2

len_l = len(d)
print(len_l)

head = create_ll(d)

for _ in range(10):  # this is done for mixing 10 times
    # for each node.index we have to now move the node
    # node.index stores the order in which the node are supposed to move
    # if the order changes the result will change
    for index in range(len(d)):
        # first get the node which we are supposed to move
        # assign the node as head node for ease
        head = get_node_at_index(head, index)
        # if data in the node is not equal to zero only then move the node
        if head.x != 0:
            # remove the node at the index
            head, data = remove_node(head, 0)
            # we use modulus operation to find the number of moves for the node
            # indx2move stores how many steps the node will move
            if math.fabs(data) > len_l:  # check if the data is greater than the list size
                indx_2_move = math.fabs(data) % (len_l - 1)
                indx_2_move = indx_2_move * -1 if data < 0 else indx_2_move
            else:
                indx_2_move = data  # if data is not greater than list size then ind2move is the data itself

            # print(f"removed {data}, index2move {indx_2_move}")
            add_node(head, indx_2_move, data,
                     index)  # now we add the node with reference to head . index is passed to the function to preserve the original order in which nodes are required to mix
            # print_ll(head)
    print("round of mixing done")

# find the node with data  = 0
node0 = head
while node0.x != 0:
    node0 = node0.next

a, b, c = 0, 0, 0
node = node0
# this loop finds the node data with reference to node(zero) AT 1000TH, 2000TH, 3000TH place
for i in range(1, 3001):
    if i == 1001:
        a = node.x
    if i == 2001:
        b = node.x
    if i == 3000:
        c = node.next.x

    node = node.next

print("sum part 2")
print(a + b + c)

