with open("INPUT DAY 2.txt", "rt") as myfile:
    lines = myfile.readlines()

new_lines = []
for line in lines:
    if line[0] in ['A', 'B', 'C']: #iterating over each line and checking if any of it contains A,B,C

        new_word_with_only_ABCXYZ = '' #making an empty word
        for letter in line: #checking for a ABCXYZ words only
            if letter in "ABCXYZ": #if found adding the ABCXYZ to the new word empty list
                new_word_with_only_ABCXYZ = new_word_with_only_ABCXYZ + letter
        new_lines.append(new_word_with_only_ABCXYZ) #and appending it to new lines list in which we will have [AX, AY...]

# Dictionary to store individual move scores
score_dict = {
    "A": 1,  # Rock
    "B": 2,  # Paper
    "C": 3,  # Scissors
    "X": 1,  # Rock
    "Y": 2,  # Paper
    "Z": 3  # Scissors
}

total_score = 0

for item in new_lines: #now looping in our req list which has pairs
    #Draw
    if (score_dict[item[0]] - score_dict[item[1]]) == 0: #in the score dict [AY] the first item[0] is A AND ITEM[1] is Y; checking weather if the diff is zero
        total_score += 3 + score_dict[item[1]] #adding 3 to the sign OF SECOND ITEM
    #Win
    elif (score_dict[item[0]] - score_dict[item[1]]) in [-1, 2]:  #CHECKING IF THE DIFF IS EITHER -1 OR 2 IN WINNING CASES
        total_score += 6 + score_dict[item[1]] #ADDNG 6 TO THE SCORE OF ITEM
    #Lose
    else: #THE REST O FTHE CASES ARE LOSE
        total_score += score_dict[item[1]] #HENCE ADDING THE SCORE O FTHE ITEM ONLY

print(total_score)

#####PART2########
score_dict_2 = {
    "A": 1,  # Rock
    "B": 2,  # Paper
    "C": 3,  # Scissors
    "X": 0,  #LOSE
    "Y": 3,  # DRAW
    "Z": 6,  #WIN
}


total_score = 0

for item in new_lines:
    #Draw
    if item[1] == "Y": #if the game is draw ==y
        total_score = total_score + score_dict_2[item[0]] + score_dict_2["Y"] #adding the sum of item0 and the score of draw
    #Win
    elif item[1] == "Z": #if the game is win for us then it has to be with z (second letter)
        if item[0] == "C": #checking whether it is a  C (first) ; If CZ
            total_score = total_score + score_dict_2["A"] + score_dict_2["Z"] #adding the score of A and Z
        else:
            total_score = total_score + score_dict_2[item[0]] + 1 + score_dict_2["Z"] #else add score of first item and score of y
    #Lose "X"
    else:
        if item[0] == "A": #if the first item is A
            total_score = total_score + score_dict_2["C"] #then add the score of C (LOSING WITH SCISSORS)
        else:
            total_score = total_score + score_dict_2[item[0]] - 1 # ELSE ADD THE SCORE OF FIRST AND SUBTRACTING 1 TO IT

print(total_score)