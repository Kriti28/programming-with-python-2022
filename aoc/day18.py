class Puzzle:
    def __init__(self):
        self.points = [] #making a empty list of points

    def add(self, x, y, z):
        self.points.append((x, y, z)) #appeninding the position in points

    def calculate_Surface_Area(self): #defining a function to cal surface area
        area = 6 * len(self.points) #the area which are free are 6
        for point in self.points: #looping over each point and and their neighbour
            x, y, z = point #considering the points
            if (x+1, y, z) in self.points: #neighbour of x
                area = area - 2 #subtracting the both neighbouring sides of self and adjacent side at once
            if (x, y+1, z) in self.points: #neighbour of y
                area = area - 2
            if (x, y, z+1) in self.points: #neighbour of z
                area = area - 2
        return area
    
    def ans(self):
        return self.calculate_Surface_Area()

        # Creating instance of Puzzle
game = Puzzle()

# Read input files
file = open('day18.txt', 'r')
Lines = file.readlines()

# Reading movement of rope line by line
for line in Lines:
    L = line.split(",") #spilitting at ,
    game.add(int(L[0]), int(L[1]), int(L[2])) #taking the integer

# printing the answer
print(game.ans())
