with open("day07.txt") as file:
    data = file.readlines()

# / means root directory
all_directory = {"/": 0}
current_path = "/"

# Process every command
for cmd in data:

    # this means its a command
    if cmd[0] == "$":

        # its a list command
        if cmd[2:4] == "ls":
            pass
        # else its a change directory command
        elif cmd[2:4] == "cd":
            if cmd[5:6] == "/":
                current_path = "/"
            elif cmd[5:7] == "..":
                current_path = "/".join(current_path.split('/')[:-1])
            else:
                dir_name = cmd[5:]  # Get name of directory
                current_path += "/" + dir_name  # Add to the path
                all_directory[current_path] = 0  # Update our dictionary

    # Do nothing when listing directories available
    elif cmd[0:3] == "dir":
        pass
    else:
        size = int(cmd.split(" ")[0])

        # Update size for every directory down to /root
        temp_dir = current_path
        for i in range(current_path.count("/")):
            all_directory[temp_dir] += size
            temp_dir = "/".join(temp_dir.split('/')[:-1])

total = 0

# space required - space unused (total space - space used)
free_space = 30000000 - (70000000 - all_directory["/"])
free_space_directories = []

# Iterate through every path
for dir in all_directory:

    if all_directory[dir] < 100000:
        total += all_directory[dir]

    if free_space <= all_directory[dir]:
        free_space_directories.append(all_directory[dir])

print(total)
print(min(free_space_directories))