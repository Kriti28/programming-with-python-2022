with open("day05.txt", "rt") as myfile:
    lines = myfile.readlines()

# making dictionary of stacks
stacks = {
    1: ['D', 'B', 'J', 'V'],
    2: ['P', 'V', 'B', 'W', 'R', 'D', 'F'],
    3: ['R', 'G', 'F', 'L', 'D', 'C', 'W', 'Q'],
    4: ['W', 'J', 'P', 'M', 'L', 'N', 'D', 'B'],
    5: ['H', 'N', 'B', 'P', 'C', 'S', 'Q'],
    6: ['R', 'D', 'B', 'S', 'N', 'G'],
    7: ['Z', 'B', 'P', 'M', 'Q', 'F', 'S', 'H'],
    8: ['W', 'L', 'F'],
    9: ['S', 'V', 'F', 'M', 'R'],
}

for line in lines:
    line = line.replace('move', '').replace('from', '').replace('to', '').strip('\n') #removing all words and taking numbers into account and stripping the next line to make for ex  ' 7  7  1'
    moves = line.split(' ') #there are two spaces between 7 and 7, 7 and 1 ,so it will split on those spaces and since there's nothing between those double spaces it will put ''; ['', '7', '', '7', '', '1']
    moves = [int(i) for i in moves if i != ''] #if items in line is not equal to '' (empty space) then int the item ; [7,7,1]

    for i in range(0, moves[0]): #looping for moves[0] no of times ;moves = [7,7,1], moves[0] = 7, so we have to take out crate from top 7 times.
        stacks[moves[2]].append(stacks[moves[1]].pop()) #pop will just remove last element from our source stack and we are just appending that in our destination stack
#Print last element of each stack
print("part1")
print(stacks[1][-1])
print(stacks[2][-1])
print(stacks[3][-1])
print(stacks[4][-1])
print(stacks[5][-1])
print(stacks[6][-1])
print(stacks[7][-1])
print(stacks[8][-1])
print(stacks[9][-1])
print("\n")



print('Comment PART 1 while running part 2. Otherwise, following answer for PART 2 might not be correct.')

# Part 2

for line in lines:
    line = line.replace('move', '').replace('from', '').replace('to', '').strip('\n')
    moves = line.split(' ')
    moves = [int(i) for i in moves if i != '']

    stacks[moves[2]] = stacks[moves[2]] + stacks[moves[1]][-moves[0]:] #here in the destination stack; adding the crates from the source's stack and the -moves[0] picks the last mentioned stacks
    stacks[moves[1]] = stacks[moves[1]][:-moves[0]]  #removing all those crates from our source stack

print("part2")
print(stacks[1][-1])
print(stacks[2][-1])
print(stacks[3][-1])
print(stacks[4][-1])
print(stacks[5][-1])
print(stacks[6][-1])
print(stacks[7][-1])
print(stacks[8][-1])
print(stacks[9][-1])





