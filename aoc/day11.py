class Monkey: #making an instance for monkey
    def __init__(self, items, operation, operand, divisible_check, true_monkey, false_monkey):
        self.items = items #the list of items
        self.operation = operation #operatin of how worry level changes  which is the +, -, x
        self.operand = operand #these arre the numb after operation
        self.divisible_check = divisible_check #to check divisibility
        self.true_monkey = true_monkey #for true monkey actions
        self.false_monkey = false_monkey #for false monkey actions
        self.inspect_num = 0 #assigning a variable to know the count
        
    def inspect(self, old): #function for inspecting old operand
        if str(self.operand) == "old": #if the operand is "old" ; in case: new= old * old
            if (self.operation == "+"): #if it a plus operation
                return 2 * old #do this ; multiply two times
            elif (self.operation == "*"): #if multiply
                return old * old #multiply old to old
        elif (self.operation == "+"): # if add
            return old + int(self.operand) #add the int to old
        elif (self.operation == "*"): #if multiply
            return old * int(self.operand) #multiply the operand
        return old
        
class Puzzle: #generating the list of monkey 0,1,2,3......
    def __init__(self):
        self.monkey = [] #making a empty list that will be a object for class monkey

    def add(self, obj): #initialising the object and appending in the monkey list
        self.monkey.append(obj)
    
    def ans(self): #from the list reading monkey
        for _ in range(20): #for 20 cycles
            for i in range(len(self.monkey)): #looping into the list if monkeys
                while len(self.monkey[i].items) != 0: #until the current list is not emptied the loop will not stop
                    # increasing inspect of monkey for each inspection
                    self.monkey[i].inspect_num = self.monkey[i].inspect_num + 1
                    
                    curr_item = self.monkey[i].items.pop(0) #in the monkey list pop the first element
                    curr_item = self.monkey[i].inspect(curr_item) // 3 #inspect func wil check the operand and operation and divide it by 3
                    #updated current item

                    #divisibility check
                    if (curr_item % self.monkey[i].divisible_check) == 0: #if the updated current item is dividsible
                        self.monkey[self.monkey[i].true_monkey].items.append(curr_item) #using the index of true monkey; getting the object of monkey;get the list;
                                                                                       #and appending it to current item

                    else:
                        self.monkey[self.monkey[i].false_monkey].items.append(curr_item) #for false case
        
        # finding 2 largest inspect out of all monkey's inspect
        lst = [] #making an empty list
        for i in range(len(self.monkey)):  #traversing at each monkey
            lst.append(self.monkey[i].inspect_num ) #getting the inspect_num of each monkey and appending it to list
            
        lst.sort(reverse=True) #sorting in descending order
        print(lst)    
        print(lst[0] * lst[1]) #multiplication top 2


# Creating instance of Puzzle
game = Puzzle()

file1 = open('day11.txt', 'r')
Lines = file1.readlines()
#preprocessing of data
i = 0
while i < len(Lines):
    if (len(Lines[i].split())) == 0: #ignoring the first line ; Monkey 0:
        i = i + 1
        continue
    
    i = i + 1
    lst = Lines[i].split(":") #getting the list of items
    items = [int(i) for i in lst[1].split(",")] #splitting at , and makingn it int
    
    i = i + 1
    operation = Lines[i].split()[4] #operation at 4 position  plus, minus
    operand = Lines[i].split()[5]#operand at next position 5
    
    i = i + 1
    divisible_check = int(Lines[i].split()[3]) #divisibilty check at line pos 4 ;integer getting at this index
    
    i = i + 1
    true_monkey = int(Lines[i].split()[5]) #true monkey operation at position 5
    
    i = i + 1
    false_monkey = int(Lines[i].split()[5]) #false monkey integer pos
    
    i = i + 1
    game.add(Monkey(items, operation, operand, divisible_check, true_monkey, false_monkey)) #calling the constructor ; monkey object
    
# printing the answer
game.ans()

#####PART2#####

class Monkey:  # making an instance for monkey
    def __init__(self, items, operation, operand, divisible_check, true_monkey, false_monkey):
        self.items = items  # the list of items
        self.operation = operation  # operation of how worry level changes  which is the +, -, x
        self.operand = operand  # these are the numb after operation
        self.divisible_check = divisible_check  # to check divisibility
        self.true_monkey = true_monkey  # for true monkey actions
        self.false_monkey = false_monkey  # for false monkey actions
        self.inspect_num = 0  # assigning a variable to know the count

    def inspect(self, old):  # function for inspecting old operand
        if str(self.operand) == "old":  # if the operand is "old" ; in case: new= old * old
            if (self.operation == "+"):  # if it a plus operation
                return 2 * old  # do this ; multiply two times
            elif (self.operation == "*"):  # if multiply
                return old * old  # multiply old to old
        elif (self.operation == "+"):  # if add
            return old + int(self.operand)  # add the int to old
        elif (self.operation == "*"):  # if multiply
            return old * int(self.operand)  # multiply the operand
        return old


class Puzzle:
    def __init__(self):
        self.monkey = [] #empty list of monkeys
        self.MOD = -1 #LCM=MOD INITI0LIZING WITH -1

    def add(self, obj):
        self.monkey.append(obj)

    def ans(self):
        for _ in range(10000): #for 10000 cycles
            for i in range(len(self.monkey)):
                while len(self.monkey[i].items) != 0:
                    # increasing inspect of monkey for each inspection
                    self.monkey[i].inspect_num = self.monkey[i].inspect_num + 1

                    curr_item = self.monkey[i].items.pop(0) #in the monkey list pop the first element
                    curr_item = int(self.monkey[i].inspect(curr_item))
                    curr_item = curr_item % self.MOD #putting the new value as a mod; so that it does not inc

                    if (curr_item % self.monkey[i].divisible_check) == 0: # if the updated current item is dividsible
                        self.monkey[self.monkey[i].true_monkey].items.append(curr_item)  #using the index of true monkey; getting the object of monkey;get the list;
                                                                                         # and appending it to current item
                    else:
                        self.monkey[self.monkey[i].false_monkey].items.append(curr_item) #for false same

        # finding 2 largest inspect out of all monkey's inspect
        lst = []
        for i in range(len(self.monkey)):
            lst.append(self.monkey[i].inspect_num)

        lst.sort(reverse=True)
        print(lst[0] * lst[1])


def gcd(x, y): #cal hcf
    while (y):
        x, y = y, x % y
    return x


def lcm(x, y): #cal lcm
    lcm = (x * y) // gcd(x, y)
    return lcm


# Creating instance of Puzzle
game = Puzzle()

file1 = open('day11.txt', 'r')
Lines = file1.readlines()

i = 0
while i < len(Lines):
    if (len(Lines[i].split())) == 0: #ignoring the first line ; Monkey 0:
        i = i + 1
        continue

    i = i + 1
    lst = Lines[i].split(":")  # getting the list of items
    items = [int(i) for i in lst[1].split(",")]  # splitting at , and makingn it int

    i = i + 1
    operation = Lines[i].split()[4]  # operation at 4 position  plus, minus
    operand = Lines[i].split()[5]  # operand at next position 5

    i = i + 1
    divisible_check = int(Lines[i].split()[3])  # divisibilty check at line pos 4 ;integer getting at this index

    i = i + 1
    true_monkey = int(Lines[i].split()[5])  # true monkey operation at position 5

    i = i + 1
    false_monkey = int(Lines[i].split()[5])  # false monkey integer pos

    i = i + 1
    game.add(Monkey(items, operation, operand, divisible_check, true_monkey,
                    false_monkey))  # calling the constructor ; monkey object

    if game.MOD == -1: #PUTTING THE CONDITION
        game.MOD = divisible_check #TAKING THE FIRST INT OF DIVISIBILITY CHECK AS MOD;lcm=first one is 3
    else:
        game.MOD = lcm(divisible_check, game.MOD) #(new divisi:6, 3);cal culating LCM #updated lcm

# printing the answer
game.ans()
